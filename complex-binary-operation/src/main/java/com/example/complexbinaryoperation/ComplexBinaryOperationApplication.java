package com.example.complexbinaryoperation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ComplexBinaryOperationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComplexBinaryOperationApplication.class, args);
    }
}
