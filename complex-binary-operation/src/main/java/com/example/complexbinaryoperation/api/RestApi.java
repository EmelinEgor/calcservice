package com.example.complexbinaryoperation.api;

import com.example.complexbinaryoperation.logic.Operations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestApi {

    private Operations operations;

    public RestApi (Operations operations){
        this.operations = operations;
    }

    @RequestMapping(value = "/erection/{number}/{degree}", method = RequestMethod.GET)
    public ResponseEntity erection(@PathVariable double number, @PathVariable int degree) {

        return operations.erection(number,degree);
    }

    @RequestMapping(value = "/root/{number}/{degree}", method = RequestMethod.GET)
    public ResponseEntity root(@PathVariable double number, @PathVariable int degree) {

        return (number > 0)&&(degree > 0) ?
                operations.root(number,degree):
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Отрицательная степень недопустима. Введите положительное число. Сервис root");
    }
}
