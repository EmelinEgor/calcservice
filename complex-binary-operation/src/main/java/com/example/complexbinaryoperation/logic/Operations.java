package com.example.complexbinaryoperation.logic;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class Operations {
    public ResponseEntity erection (double number, int degree){
        if (degree == 0) {
            return  ResponseEntity.ok(1);
        } else if (degree > 0) {
            double result = 1 ;
            for (int i = 0; i < degree ; i++) {
                result = result * number;
            }
            return ResponseEntity.ok(result);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Отрицательная степень при попытке возведения. Воспользуйтесь методом для взятися корней. Сервис root. ");
        }
    }

    public ResponseEntity root (double number, int degree){
        return ResponseEntity.ok(Math.pow(number, 1.0 / degree));
    }
}
