package com.example.simplebinaryoperation.model;

import org.springframework.stereotype.Component;

@Component
public class Terms {
    private Double termA;
    private Double termB;

    public Double getTermA() {
        return termA;
    }

    public void setTermA(Double termA) {
        this.termA = termA;
    }

    public Double getTermB() {
        return termB;
    }

    public void setTermB(Double termB) {
        this.termB = termB;
    }

    @Override
    public String toString() {
        return "Terms{" +
                "termA=" + termA +
                ", termB=" + termB +
                '}';
    }
}
