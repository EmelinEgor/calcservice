package com.example.simplebinaryoperation.logic;

import com.example.simplebinaryoperation.model.Terms;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Operations {

    private RabbitTemplate rabbitTemplate;
    private Terms terms;

    @Autowired
    private void setRabbitTemplate (RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
    }
    @Autowired
    public void setTerms (final Terms terms){
        this.terms = terms;
    }

    private void sendMessage (RabbitTemplate rabbitTemplate, String exchange, String routingKey, Terms data) {
        rabbitTemplate.convertAndSend(exchange, routingKey, data);
    }

    public double addition (double termA, double termB){
        terms.setTermA(termA);
        terms.setTermB(termB);
        sendMessage(rabbitTemplate, "app1-exchange", "app1-routing-key", terms);
        return termA + termB;
    }

    public double subtraction (double termA, double termB){
        return termA - termB;
    }

    public double multiplication (double termA, double termB){
        return termA * termB;
    }

    public double division (double termA, double termB){
        return termA / termB;
    }


}
