package com.example.simplebinaryoperation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SimpleBinaryOperationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleBinaryOperationApplication.class, args);
    }
}
