package com.example.simplebinaryoperation.api;

import com.example.simplebinaryoperation.logic.Operations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RestApi {

    private Operations operations;

    public RestApi (Operations operations){
        this.operations = operations;
    }

    @RequestMapping(value = "/addition/{termA}/{termB}", method = RequestMethod.GET)
    public ResponseEntity addition(@PathVariable double termA, @PathVariable double termB) {

        return ResponseEntity.status(HttpStatus.OK).body(operations.addition(termA,termB));
    }

    @RequestMapping(value = "/subtraction/{termA}/{termB}", method = RequestMethod.GET)
    public ResponseEntity subtraction(@PathVariable double termA, @PathVariable double termB) {

        return ResponseEntity.status(HttpStatus.OK).body(operations.subtraction(termA,termB));
    }

    @RequestMapping(value = "/multiplication/{termA}/{termB}", method = RequestMethod.GET)
    public ResponseEntity multiplication(@PathVariable double termA, @PathVariable double termB) {

        return ResponseEntity.status(HttpStatus.OK).body(operations.multiplication(termA,termB));
    }

    @RequestMapping(value = "/division/{termA}/{termB}", method = RequestMethod.GET)
    public ResponseEntity division(@PathVariable double termA, @PathVariable double termB) {

        return termB == 0 ?
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ошибка деления на 0, невозможно выполнить операцию. Сервис groot.") :
                ResponseEntity.status(HttpStatus.OK).body(operations.division(termA,termB));
    }


}
