package com.example.solverofequations.utils;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateConstructor {
    private static final String GATEWAY_PATH = "http://localhost:10000/";
    private RestTemplate restTemplate;

    public RestTemplateConstructor() {
        this.restTemplate = new RestTemplate();
    }

    public ResponseEntity<Double> makeGetRequest(String path)throws NullPointerException{
        return restTemplate.getForEntity(GATEWAY_PATH + path, Double.TYPE);
    }
}
