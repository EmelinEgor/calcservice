package com.example.solverofequations.logic;

import com.example.solverofequations.utils.RestTemplateConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

@Service
public class Operations {
    private RestTemplateConstructor restTemplateConstructor;

    public Operations(RestTemplateConstructor restTemplateConstructor) {
        this.restTemplateConstructor = restTemplateConstructor;
    }

    public ResponseEntity resolver (double a, double b, double c){
        double result[] = {0,0};
        try {
            double b2 = restTemplateConstructor.makeGetRequest("root/erection/" + b + "/2").getBody();
            double _4a = restTemplateConstructor.makeGetRequest("groot/multiplication/" + a + "/4").getBody();
            double _4ac = restTemplateConstructor.makeGetRequest("groot/multiplication/" + _4a + "/" + c).getBody();
            double discremenant  =  restTemplateConstructor.makeGetRequest("groot/subtraction/" + (b2) + "/" + _4ac).getBody();
            if (discremenant == 0){
                result[0] = -b/(2*a);
                result[1] = result[0];
            } else if (discremenant > 0 ){
                result[0] = (-b + restTemplateConstructor.makeGetRequest("oot/root/subtraction/" + b2 + "/" + _4ac + "/2").getBody())/(2*a);
                result[1] = (-b - restTemplateConstructor.makeGetRequest("oot/root/subtraction/" + b2 + "/" + _4ac + "/2").getBody())/(2*a);
            } else {
                return ResponseEntity.ok("Корней нет");
            }
        }catch (HttpServerErrorException| HttpClientErrorException e){
            return ResponseEntity.status(e.getStatusCode()).body(e.getResponseBodyAsString());
        }catch (NullPointerException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage() + "    " + e.getCause());
        }
        return ResponseEntity.ok(result);
    }
}
