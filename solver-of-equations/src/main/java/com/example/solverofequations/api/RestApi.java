package com.example.solverofequations.api;

import com.example.solverofequations.logic.Operations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestApi {
    private Operations operations;

    public RestApi (Operations operations){
        this.operations = operations;
    }

    @RequestMapping(value = "/{termA}/{termB}/{termC}", method = RequestMethod.GET)
    public ResponseEntity erectionAddition(@PathVariable double termA, @PathVariable double termB, @PathVariable double termC) {

        return operations.resolver(termA,termB,termC);
    }
}
