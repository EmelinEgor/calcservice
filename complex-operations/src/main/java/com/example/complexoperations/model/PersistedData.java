package com.example.complexoperations.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;

@Document
public class PersistedData {

    @Field
    private double termA;
    @Field
    private double termB;
    @Field
    private int degree;

    public PersistedData( double termA, double termB, int degree) {
        this.termA = termA;
        this.termB = termB;
        this.degree = degree;
    }

    public double getTermA() {
        return termA;
    }

    public void setTermA(double termA) {
        this.termA = termA;
    }

    public double getTermB() {
        return termB;
    }

    public void setTermB(double termB) {
        this.termB = termB;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    @Override
    public String toString() {
        return "PersistedData{" +
                ", termA=" + termA +
                ", termB=" + termB +
                ", degree=" + degree +
                '}';
    }
}
