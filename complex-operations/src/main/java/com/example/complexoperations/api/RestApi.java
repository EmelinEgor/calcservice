package com.example.complexoperations.api;

import com.example.complexoperations.logic.Operations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestApi {

    private Operations operations;

    public RestApi (Operations operations){
        this.operations = operations;
    }

    @RequestMapping(value = "/erection/addition/{termA}/{termB}/{degree}", method = RequestMethod.GET)
    public ResponseEntity erectionAddition(@PathVariable double termA,@PathVariable double termB, @PathVariable int degree) {

        return operations.erectionAddition(termA,termB,degree);
    }

    @RequestMapping(value = "/root/addition/{termA}/{termB}/{degree}", method = RequestMethod.GET)
    public ResponseEntity rootAddition(@PathVariable double termA,@PathVariable double termB, @PathVariable int degree) {

        return (degree > 0) ?
                operations.rootAddition(termA,termB,degree):
                ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Допустима степень только больше 0. Сервис oot") ;
    }

    @RequestMapping(value = "/erection/subtraction/{termA}/{termB}/{degree}", method = RequestMethod.GET)
    public ResponseEntity erectionSubtraction(@PathVariable double termA,@PathVariable double termB, @PathVariable int degree) {

        return operations.erectionSubtraction(termA,termB,degree);
    }

    @RequestMapping(value = "/root/subtraction/{termA}/{termB}/{degree}", method = RequestMethod.GET)
    public ResponseEntity rootSubtraction(@PathVariable double termA,@PathVariable double termB, @PathVariable int degree) {

        return (degree > 0)?
                operations.rootSubtraction(termA,termB,degree):
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Недопустимое значение степени. введите положительное число. Сервис oot") ;
    }
}