package com.example.complexoperations.logic;

//import com.couchbase.client.deps.com.fasterxml.jackson.core.JsonProcessingException;
//import com.couchbase.client.deps.com.fasterxml.jackson.databind.ObjectMapper;
//import com.couchbase.client.java.Bucket;
//import com.couchbase.client.java.Cluster;
//import com.couchbase.client.java.CouchbaseCluster;
//import com.couchbase.client.java.document.JsonDocument;
//import com.couchbase.client.java.document.json.JsonObject;
//import com.example.complexoperations.model.PersistedData;
import com.example.complexoperations.utils.RestTemplateConstructor;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;


@Service
public class Operations {

//    private Cluster cluster = CouchbaseCluster.create("localhost");
//    private ObjectMapper mapper = new ObjectMapper();


    private static final String PATH_SIMPLE_BINARY = "/groot";
    private static final String PATH_COMPLEX_BINARY = "/root";
    private ResponseEntity tempResultSimple;
    private ResponseEntity tempResultComplex;

    private RestTemplateConstructor restTemplateConstructor;

    @Autowired
    public void setRestTemplateConstructor (RestTemplateConstructor restTemplateConstructor){
        this.restTemplateConstructor = restTemplateConstructor;
    }

    @HystrixCommand(fallbackMethod = "defaultStores")
    public ResponseEntity erectionAddition (double termA, double termB, int degree) throws NullPointerException{
//        cluster.authenticate("admin", "password");
//        Bucket bucket = cluster.openBucket("test");
//        JsonObject object = null;
//        try{
//            object = JsonObject.fromJson(mapper.writeValueAsString(new PersistedData(termA,termB,degree)));
//        }catch (JsonProcessingException e){
//            System.out.println(e.getMessage() + " " + e.getStackTrace());
//        }

//        bucket.upsert(JsonDocument.create("first_set",object));
//        System.out.println(bucket.get("first_set").content());
//        try {
            tempResultSimple = restTemplateConstructor.makeGetRequestSimple(termA, termB, PATH_SIMPLE_BINARY, 0);
            tempResultComplex = restTemplateConstructor.makeGetRequestComplex((double) tempResultSimple.getBody(), degree, PATH_COMPLEX_BINARY, 0);
//        }catch (HttpServerErrorException |HttpClientErrorException e){
//            return ResponseEntity.status(e.getStatusCode()).body(e.getResponseBodyAsString() + "Вызван сервисом oot");
//        }
//        bucket.close();
        return tempResultComplex;
    }

    public ResponseEntity rootAddition (double termA, double termB, int degree){

        try {
            tempResultSimple = restTemplateConstructor.makeGetRequestSimple(termA, termB, PATH_SIMPLE_BINARY, 0);
            tempResultComplex = restTemplateConstructor.makeGetRequestComplex((double) tempResultSimple.getBody(), degree, PATH_COMPLEX_BINARY, 1);
        }catch (HttpServerErrorException |HttpClientErrorException e){
            return ResponseEntity.status(e.getStatusCode()).body(e.getResponseBodyAsString() + "Вызван сервисом oot");
        }
        return tempResultComplex;
    }

    public ResponseEntity erectionSubtraction (double termA, double termB, int degree){
        try {
        tempResultSimple = restTemplateConstructor.makeGetRequestSimple(termA, termB, PATH_SIMPLE_BINARY, 1);
        tempResultComplex = restTemplateConstructor.makeGetRequestComplex((double) tempResultSimple.getBody(), degree, PATH_COMPLEX_BINARY, 0);
        }catch (HttpServerErrorException |HttpClientErrorException e){
            return ResponseEntity.status(e.getStatusCode()).body(e.getResponseBodyAsString() + "Вызван сервисом oot");
        }
            return tempResultComplex;
    }

    public ResponseEntity rootSubtraction (double termA, double termB, int degree){
        try {
        tempResultSimple = restTemplateConstructor.makeGetRequestSimple(termA, termB, PATH_SIMPLE_BINARY, 1);
        tempResultComplex = restTemplateConstructor.makeGetRequestComplex((double) tempResultSimple.getBody(), degree, PATH_COMPLEX_BINARY, 1);
        }catch (HttpServerErrorException |HttpClientErrorException e){
            return ResponseEntity.status(e.getStatusCode()).body(e.getResponseBodyAsString() + "Вызван сервисом oot");
        }
            return tempResultComplex;
    }

    public ResponseEntity defaultStores (double termA, double termB, int degree){
        String response = "Вы ввели следующие значения: " + termA + "/" + termB + "/" + degree + " но выполнить вычесления не удалось, скорее всего неработает один из микросервисов.";
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(response);
    }
}
