package com.example.complexoperations.utils;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateConstructor {

    private static final String PATH = "http://localhost:10000";

    private RestTemplate restTemplate;
    public  RestTemplateConstructor (){
        this.restTemplate = new RestTemplate();
    }


    public ResponseEntity makeGetRequestSimple(double termA, double termB, String path, int type) throws HttpClientErrorException, HttpServerErrorException {
        ResponseEntity result = null;
        if (type == 0) {
            result = restTemplate.getForEntity(PATH + path + "/addition/" + termA + "/" + termB, Double.TYPE);
        } else if (type == 1){
            result = restTemplate.getForEntity(PATH + path + "/subtraction/" + termA + "/" + termB, Double.TYPE);
        }
        return result;
    }

    public ResponseEntity makeGetRequestComplex(double number, int degree, String path, int type) throws HttpClientErrorException, HttpServerErrorException {
        ResponseEntity result = null;
        if(type == 0){
            result = restTemplate.getForEntity(PATH + path + "/erection/"+number+"/"+degree, Double.TYPE);
        } else if (type == 1){
            result = restTemplate.getForEntity(PATH + path + "/root/"+number+"/"+degree, Double.TYPE);
        }
        return result;

    }
}
