package com.example.complexoperations.utils;

import com.example.complexoperations.model.Terms;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class RabbitAPI {
    private Environment env;

    @Autowired
    private void setEnv (Environment env){
        this.env = env;
    }

    @RabbitListener(queues = "${app1.queue.name}")
    public void rabbitList (final Terms number){
        String s = number.toString();
        System.out.println(s);
    }

    public void sendMessage (RabbitTemplate rabbitTemplate, String exchange, String routingKey, Terms data) {
        rabbitTemplate.convertAndSend(exchange, routingKey, data);
    }

}
